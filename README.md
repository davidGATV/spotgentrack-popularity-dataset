# SpotGenTrack-Popularity-dataset

## Dataset Generation
The database was originally stored in Elasticsearch after collecting the data from both Spotify and Genius.
In this repository, we have included a .zip file with five csv files that contain the distinct datasets.


## 1) spotify_tracks.csv
This file contains all the information regarding each Track element. Column 'id' is the unique identifier of each element. 
The following columns form this database:

				'acousticness',
				'album_id',
				'analysis_url',
				'artists_id',
				'available_markets',
				'country',
				'danceability',
				'disc_number',
				'duration_ms',
				'energy',
				'href',
				'id',
				'instrumentalness',
				'key',
				'liveness',
				'loudness',
				'lyrics',
				'mode',
				'name',
				'playlist',
				'popularity',
				'preview_url',
				'speechiness',
				'tempo',
				'time_signature',
				'track_href',
				'track_name_prev',
				'track_number',
				'uri',
				'valence',
				'type'

## 2) spotify_artists.csv
This file contains all the information regarding each Artist element. Column 'id' is the unique identifier of each element. 
The following columns form this database:

				'artist_popularity',
				'followers',
				'genres',
				'id',
				'name',
				'track_id',
				'track_name_prev',
				'type'
## 3) spotify_albums.csv
This file contains all the information regarding each Album element. Column 'id' is the unique identifier of each element. 
The following columns form this database:

				'album_type',
				'artist_id',
				'available_markets',
				'external_urls',
				'href',
				'id',
				'images',
				'name',
				'release_date',
				'release_date_precision',
				'total_tracks',
				'track_id',
				'track_name_prev',
				'uri',
				'type'
## 4) low_level_audio_features.csv
This file the low level features extracted from the collection of preview_URL's. Dirty data was removed.
There is a column named as 'track_id' that links the features to the corresponding Track. This database contains a large number of columns.

## 5) lyrics_features.csv
This file contains the text features that are extracted from Lyrics. Dirty data was removed. TThe following columns form this database:

				'mean_syllables_word',
				'mean_words_sentence',
				'n_sentences',
				'n_words',
				'sentence_similarity',
				'track_id',
				'vocabulary_wealth'